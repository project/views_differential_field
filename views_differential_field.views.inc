<?php

/**
 * @file
 * Implements hook_views_data().
 */

/**
 * Implements hook_views_data().
 */
function views_differential_field_views_data() {
  $data['views_differential_field']['table']['group'] = t('Global');
  $data['views_differential_field']['table']['join'] = [
    // Exist in all views.
    '#global' => [],
  ];
  $data['views_differential_field']['field_differential_field'] = [
    'title' => t('Differntial Field'),
    'help' => t('Views field that calculates the differential value of another field in your view.'),
    'field' => [
      'id' => 'field_differential_field',
    ],
  ];
  return $data;
}
